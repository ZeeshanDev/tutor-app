import React from 'react'
import { Col, Row, FormLabel, Modal } from 'react-bootstrap'
import './SubjQuiz.css'
import graph from '../../../../../images/graph.png'

export default function SubjQuiz () {


    return (



        <>
            <div className="container SQ-container" style={{background:"#F6F7F9"}}>
                <Row className="py-3" style={{background:"#005D8A"}}>
                    <Col className="col-md-6 text-center" style={{borderRight:"#EFEFF6 solid 1px"}}>
                        <div><h5 className="QT">1/10 Question</h5></div>
                    </Col>
                    <Col className="col-md-6 text-center" style={{borderLeft:"#EFEFF6 solid 1px"}}>
                        <div><h5 className="QT">0:57:17 Timing</h5></div>
                    </Col>
                </Row>
                
                <div className="row SQ-Questions py-3">
                    <div className="col-md-12" style={{lineHeight:"2"}}>
                        <div className="py-2">
                            
                            <p className="PQuestion">What is a difference between Canvas and Artboard in Adobe XD?</p>
                        </div>
                        <div className="py-2 Pdiv my-2 pl-2">
                            <p className="PAnswer">The Canvas can have zero, one or more Artboards placed on it</p>
                        </div>
                        <div className="py-2 Pdiv my-2 pl-2">
                            <p className="PAnswer">Imagine the canvas is a table and each artboard is a sheet of paper. </p>
                        </div>
                        <div className="py-2 Pdiv my-2 pl-2">
                            <p className="PAnswer">You can organize the artboards on the canvas the same way as you can organize .</p>
                        </div>
                        <div className="py-2 Pdiv my-2 pl-2">
                            <p className="PAnswer">All abobe</p>
                        </div>
                    </div>
                    {/* <Col className="col-md-2 d-flex flex-column justify-content-end">
                        <div className="form-check">
                            <ul>
                                <li><input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" defaultChecked/></li>
                                <li><input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2"/></li>
                                <li><input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault3"/></li>
                                <li><input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault4"/></li>
                            </ul>
                            
                        </div>
                    </Col> */}
                </div>
                <div className="d-flex justify-content-end py-2">
                    <button className="btn" style={{color:"white",background:"#05CC69",fontSize:"12px",fontFamily:"Poppins"}}>Next Question</button>
                </div>
            </div>



            {/* ----------------model----------- */}

            <Modal className="SubjQ-Modal">
                    <Modal.Header closeButton>

                    </Modal.Header>
                    <Modal.Body>
                        <Row className="d-flex justify-content-center">
                            <div><img src={graph}></img></div>
                        </Row>
                        <Row className="d-flex justify-content-center flex-column py-4">
                            <div className="text-center"><h5 className="title">7 / 10</h5></div>
                            <div className="text-center"><p className="text">You have done a great attempt, try <br/> anther course  and level up your skills & knowledge</p></div>
                        </Row>
                        <Row className="d-flex justify-content-center">
                            <div><button className="btn button">Back to Dashboard</button></div>
                        </Row>
                    </Modal.Body>
                    
                </Modal>

        </>

    )
 
}