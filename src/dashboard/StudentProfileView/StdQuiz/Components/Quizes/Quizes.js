import React, { useState } from 'react'
import './Quizes.css'
import Ellipse62 from '../../../../../images/Ellipse62.png'
import questionanswer from '../../../../../images/questionanswer.png'
import timer from '../../../../../images/timer.png'
import Group3dots from '../../../../../images/Group3dots.png'
import CQuizes from './Component/CQuizes'


export default function Quizes () {

    


    const ArraylistQuizes = [
        {
            id:1,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",

        },
        {
            id:2,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",

        },
        {
            id:3,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",

        },
        {
            id:4,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",

        },
        {
            id:5,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",

        },
    ]

    // "arraylistarg" is a variable name which can holds Array current values and "i" is index
    const QuizesList = ArraylistQuizes.map( (arg, i) => {
        return (
            <CQuizes key={i} 
            id="i" 
            Ellipse62={ArraylistQuizes[i].Ellipse62}
            title={ArraylistQuizes[i].title}
            questionanswer={ArraylistQuizes[i].questionanswer} 
            qa={ArraylistQuizes[i].qa} 
            timer={ArraylistQuizes[i].timer} 
            time={ArraylistQuizes[i].time}
            Group3dots={ArraylistQuizes[i].Group3dots}
            />
        )
    })




    return (


        <>
            <div className="container Quizes-container">
                
                {QuizesList}
                {/* <SubjQuiz/> */}
                {/* <NewQuiz/> */}


                

            {/*--------------pagination-------------------  */}
                {/* <div className="py-3 d-flex justify-content-center CQuizes-pagination">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                </div> */}

                

            </div>

        </>

    )

}