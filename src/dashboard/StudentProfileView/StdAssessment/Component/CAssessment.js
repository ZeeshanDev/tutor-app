import React, {useState} from 'react'
import './CAssessment.css'
import Ellipse62 from '../../../../images/Ellipse62.png'
import pdf from '../../../../images/pdf.png'
import timer from '../../../../images/timer.png'
import Group3dots from '../../../../images/Group3dots.png'
import CCAssessment from './Component/CCAssessment'
// import Modal from 'react-modal'
import { Button, Form, FormLabel, Modal } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import attach from '../../../../images/attach.png'

export default function CAssessment () {

    const [modalState, setModalState] = useState(false);

    const manageState = () => {
        setModalState(!modalState)
    }


    const ArraylistCAssessment = [
        {
            id:1,
            Ellipse62: Ellipse62,
            title: "Student Name",
            Group3dots : Group3dots,
            timer: timer,
            pdf: pdf,
            FileName: "File Name",
            time: "1 week",
            date: "05/05/2021",
            

        },
        {
            id:2,
            Ellipse62: Ellipse62,
            title: "Student Name",
            Group3dots : Group3dots,
            timer: timer,
            pdf: pdf,
            FileName: "File Name",
            time: "1 week",
            date: "05/05/2021",
            

        },
        {
            id:3,
            Ellipse62: Ellipse62,
            title: "Student Name",
            Group3dots : Group3dots,
            timer: timer,
            pdf: pdf,
            FileName: "File Name",
            time: "1 week",
            date: "05/05/2021",
            

        },
        {
            id:4,
            Ellipse62: Ellipse62,
            title: "Student Name",
            Group3dots : Group3dots,
            timer: timer,
            pdf: pdf,
            FileName: "File Name",
            time: "1 week",
            date: "05/05/2021",
            

        },
        {
            id:5,
            Ellipse62: Ellipse62,
            title: "Student Name",
            Group3dots : Group3dots,
            timer: timer,
            pdf: pdf,
            FileName: "File Name",
            time: "1 week",
            date: "05/05/2021",
            

        },
    ]

    // "arraylistarg" is a variable name which can holds Array current values and "i" is index
    const CAssessmentList = ArraylistCAssessment.map( (arg, i) => {
        return (
            <CCAssessment key={i} 
            id="i" 
            Ellipse62={ArraylistCAssessment[i].Ellipse62}
            title={ArraylistCAssessment[i].title}
            pdf={ArraylistCAssessment[i].pdf} 
            FileName={ArraylistCAssessment[i].FileName} 
            timer={ArraylistCAssessment[i].timer} 
            time={ArraylistCAssessment[i].time}
            date={ArraylistCAssessment[i].date}
            Group3dots={ArraylistCAssessment[i].Group3dots}
            />
        )
    })




    return (


        <>
            <div className="container CAssessment-container">
                {CAssessmentList}


                <div className="py-3 d-flex justify-content-center CA-pagination">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                </div>

                {/* <div className="pb-3 d-flex justify-content-end">
                    <button className="btn CAbutton" onClick={() => manageState()}>Create New Quiz</button>
                </div> */}


                {/* <div>
                        
                    <Modal className="cstm-CAModal" show={modalState} onHide={() => manageState()}>
                        <Modal.Header closeButton>
                            <div className="row col-md-12 d-flex justify-content-center">
                                <div><h5 className="CAMheader">Assign Documents to students</h5></div>
                                
                            </div>
                        </Modal.Header>
                        <Modal.Body>
                            <Form>

                                <div className="row d-flex mb-3 justify-content-center">
                                    <div className="d-flex col-md-10">
                                        <FormLabel className="CAMlabel col-md-3" htmlFor="coursecategory">Title</FormLabel>
                                        <select className="CAMfield col-md-9" name="year" id="coursecategory" placeholder="Student id">
                                            <option value="1990">1990</option>
                                            <option value="1991">1991</option>
                                            <option value="1992">1992</option>
                                            <option value="1993">1993</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row d-flex mb-3 justify-content-center">
                                    <div className="d-flex col-md-10">
                                        <FormLabel className="CAMlabel col-md-3" htmlFor="coursecategory">Student</FormLabel>
                                        <select className="CAMfield col-md-9" name="year" id="coursecategory" placeholder="Student id">
                                            <option value="1990">1990</option>
                                            <option value="1991">1991</option>
                                            <option value="1992">1992</option>
                                            <option value="1993">1993</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row d-flex mb-3 justify-content-center">
                                    <div className="d-flex col-md-10">
                                        <FormLabel className="CAMlabel col-md-3" htmlFor="coursecategory">Duration</FormLabel>
                                        <select className="CAMfield col-md-9" name="year" id="coursecategory" placeholder="Student id">
                                            <option value="1990">1990</option>
                                            <option value="1991">1991</option>
                                            <option value="1992">1992</option>
                                            <option value="1993">1993</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row d-flex mb-3 justify-content-center">
                                    <div className="d-flex">
                                    <Link to="#/" className="btn CAMbutton-red"><img src={attach}></img>File Attach</Link>
                                    </div>
                                </div>
                            </Form>
                        </Modal.Body>
                        <Modal.Footer><Button className="CAMbutton">Save</Button></Modal.Footer>
                        
                    </Modal>
                </div> */}

            </div>

        </>

    )

}