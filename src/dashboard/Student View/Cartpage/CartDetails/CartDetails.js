import React from 'react'
import { Row } from 'react-bootstrap'
import './CartDetails.css'
import card1 from '../../../../images/card1.png'
import card2 from '../../../../images/card2.png'
import card3 from '../../../../images/card3.png'
import card4 from '../../../../images/card4.png'

export default function CartDetails() {


    return (

        <>
            <div className="card CDcontainer">
                <div className="card-body p-5">
                    <Row className="py-2">
                        <div><h5 className="tp">Cart Total</h5></div>
                    </Row>
                    <Row className="d-flex justify-content-between align-items-center py-2">
                        <div><p className="cardtype">Card Type</p></div>
                        <div className="d-flex">
                            <span><img src={card1}></img></span>
                            <span><img className="px-2" src={card2}></img></span>
                            <span><img className="px-2" src={card4}></img></span>
                            <span><img src={card4}></img></span>
                        </div>
                    </Row>
                    <Row className="d-flex flex-column py-2">
                        
                            <input className="input my-1" type="text" placeholder="Name on card"></input>
                        
                            <input className="input my-1" type="text" placeholder="Card number"></input>
                        
                            <input className="input my-1" type="text" placeholder="Expiration date"></input>
                    
                        
                            <input className="input my-1" type="text" placeholder="Security code"></input>
                    </Row>
                    <Row className="d-flex justify-content-between align-items-center py-2">
                        <div><p className="tp">Total:</p></div>
                        <div><p className="tp">$49.65</p></div>
                    </Row>
                    <Row className="py-2">
                        <button className="btn button col-md-12">Pay Now</button>
                    </Row>
                </div>
            </div>
        </>

    )

}