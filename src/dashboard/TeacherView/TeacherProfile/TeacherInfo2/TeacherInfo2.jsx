import React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import './TeacherInfo2.css';
import { Col, Form, FormLabel, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import EditIcon from '../../../../assets/images/editicon.png';
import Facebook from '../../../../assets/images/facebook1.png'
import Twitter from '../../../../assets/images/twitter.png'
import Linkedin from '../../../../assets/images/linkedin1.png'

export default function TeacherInfo2() {



    return (
        <div className="TI2-containerfluid container-fluid p-3">
            <div className="TI2-container container">
            
                <Form>
                    <Col className="TI2-C1">
                        <Row className="TI2-C1-R1">
                            <h4 className="flex-grow-1">Teacher Info</h4>
                            <Link to="/#"><button className="btn my-button">Save</button></Link>
                        </Row>
                        <Row className="TI2-C1-R2 py-3">
                            <Row>
                                <div className="form-group d-flex">
                                    <Col className="col-md-5">
                                        <FormLabel htmlFor="skillset">Skill Set :</FormLabel>
                                    </Col>
                                    <Col className="col-md-7">
                                        <Row><input type="text" className="form-control" placeholder="" id="skillset" /></Row>
                                    </Col>
                                </div>
                            </Row>
                            <Row>
                                <div className="form-group d-flex">
                                    <Col className="col-md-5">
                                        <FormLabel>Exp :</FormLabel>
                                    </Col>
                                    <Col className="col-md-7">
                                        <Row className="d-flex flex-column">
                                            <FormLabel htmlFor="experience">Experience</FormLabel>
                                            <input type="text" className="form-control" placeholder="" id="experience" />
                                        </Row>
                                        <Row className="d-flex flex-row">
                                            <div className="d-flex flex-column col-md-6 p-0">
                                                <FormLabel htmlFor="year">Year</FormLabel>
                                                <select name="year" id="year" placeholder="1990">
                                                    <option value="1990">1990</option>
                                                    <option value="1991">1991</option>
                                                    <option value="1992">1992</option>
                                                    <option value="1993">1993</option>
                                                </select>
                                            </div>
                                            <div className="col-md-6">
                                                <Link to="/#"><button className="btn my-button">Add more</button></Link>
                                            </div>                                            
                                      </Row>                                       
                                    </Col>
                                </div>
                            </Row>
                            <Row>
                                <div className="form-group d-flex">
                                    <Col className="col-md-5">
                                        <FormLabel>Education :</FormLabel>
                                    </Col>
                                    <Col className="col-md-7">
                                        <Row className="d-flex flex-column">
                                            <FormLabel htmlFor="degree">Degree</FormLabel>
                                            <select name="degree" id="degree" placeholder="Select your degree">
                                                    <option value="BSCS">BSCS</option>
                                                    <option value="BBA">BBA</option>
                                                    <option value="Medical">Medical</option>
                                                    <option value="M.A">M.A</option>
                                                </select>
                                        </Row>
                                        <Row className="d-flex flex-column">
                                            <FormLabel htmlFor="institute">Institute</FormLabel>
                                                <select name="institute" id="institute" placeholder="Select your institute">
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="C">C</option>
                                                    <option value="D">D</option>
                                                </select>
                                        </Row>
                                        <Row className="d-flex flex-row">
                                            <div className="d-flex flex-column col-md-6 p-0">
                                                <FormLabel htmlFor="edulvl">Education level</FormLabel>
                                                <select name="edulvl" id="edulvl" placeholder="Graduation">
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="C">C</option>
                                                    <option value="D">D</option>
                                                </select>
                                            </div>
                                            <div className="d-flex flex-column col-md-6">
                                                <FormLabel htmlFor="year1">Year</FormLabel>
                                                <select name="year1" id="year1" placeholder="1990">
                                                    <option value="1990">Volvo</option>
                                                    <option value="1991">Saab</option>
                                                    <option value="1992">Opel</option>
                                                    <option value="1993">Audi</option>
                                                </select>
                                            </div>
                                        </Row>                                        
                                    </Col>
                                </div>
                            </Row>
                            <Row>
                                <div className="form-group d-flex">
                                    <Col className="col-md-5">
                                        <FormLabel>Language :</FormLabel>
                                    </Col>
                                    <Col className="col-md-7">                                       
                                        <Row className="d-flex flex-row">
                                            <div className="d-flex flex-column col-md-6 p-0">
                                                <FormLabel htmlFor="language">Select Language</FormLabel>
                                                <select name="language" id="language" placeholder="English">
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="C">C</option>
                                                    <option value="D">D</option>
                                                </select>
                                            </div>
                                            <div classname="col-md-6">
                                            <Link to="/#"><button className="btn my-button">Add more</button></Link>
                                            </div>                                            
                                      </Row>                                       
                                    </Col>
                                </div>
                            </Row>
                            <Row>
                                <div className="form-group d-flex">
                                    <Col className="col-md-5">
                                        <FormLabel htmlFor="other">Other :</FormLabel>
                                    </Col>
                                    <Col className="col-md-7">
                                        <Row><input type="text" className="form-control" placeholder="" id="other" /></Row>
                                    </Col>
                                </div>
                            </Row>                          
                        </Row>
                        
                        <Row className="TI2-C1-R3">
                            <h5>Social Links</h5>
                            <Row>
                                <Link to='/#'><img src={Facebook}></img></Link>
                                <Link to='/#'><img src={Linkedin}></img></Link>
                                <Link to='/#'><img src={Twitter}></img></Link>
                            </Row>
                        </Row>
                    </Col>
                </Form>
            </div>
        </div>
    )

}