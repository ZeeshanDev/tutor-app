import React from 'react'
import './QuizResult.css'
import Ellipse62 from '../../../../../images/Ellipse62.png'
import questionanswer from '../../../../../images/questionanswer.png'
import timer from '../../../../../images/timer.png'
import Group3dots from '../../../../../images/Group3dots.png'
import check from '../../../../../images/check.png'
import cross from '../../../../../images/cross.png'
import list from '../../../../../images/list.png'
import CQuizResult from './Component/CQuizResult'

export default function QuizResult () {


    const ArraylistQuizResult = [
        {
            id:1,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",
            check: check,
            cross: cross,
            list: list,
            one: "1",
            nine: "9",
            per: "90%",

        },
        {
            id:2,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",
            check: check,
            cross: cross,
            list: list,
            one: "1",
            nine: "9",
            per: "90%",

        },
        {
            id:3,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",
            check: check,
            cross: cross,
            list: list,
            one: "1",
            nine: "9",
            per: "90%",

        },
        {
            id:4,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",
            check: check,
            cross: cross,
            list: list,
            one: "1",
            nine: "9",
            per: "90%",

        },
        {
            id:5,
            Ellipse62: Ellipse62,
            title: "English Basic quiz.",
            Group3dots : Group3dots,
            timer: timer,
            questionanswer: questionanswer,
            qa: "10",
            time: "10 min",
            check: check,
            cross: cross,
            list: list,
            one: "1",
            nine: "9",
            per: "90%",

        },
    ]

    // "arraylistarg" is a variable name which can holds Array current values and "i" is index
    const QuizResultList = ArraylistQuizResult.map( (arg, i) => {
        return (
            <CQuizResult key={i} 
            id="i" 
            Ellipse62={ArraylistQuizResult[i].Ellipse62}
            title={ArraylistQuizResult[i].title}
            questionanswer={ArraylistQuizResult[i].questionanswer} 
            qa={ArraylistQuizResult[i].qa} 
            timer={ArraylistQuizResult[i].timer} 
            time={ArraylistQuizResult[i].time}
            check={ArraylistQuizResult[i].check}
            cross={ArraylistQuizResult[i].cross}
            list={ArraylistQuizResult[i].list}
            one={ArraylistQuizResult[i].one}
            nine={ArraylistQuizResult[i].nine}
            per={ArraylistQuizResult[i].per}
            Group3dots={ArraylistQuizResult[i].Group3dots}
            />
        )
    })




    return (


        <>
            <div className="container QuizResult-container">
                {QuizResultList}


                <div className="py-3 d-flex justify-content-center CQR-pagination">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>

        </>

    )

}