import React from 'react'
import { Col } from 'react-bootstrap'
import './SubjQuiz.css'

export default function SubjQuiz () {


    return (


        <>
            <div className="container SQ-container">
                <div className="row py-3 d-flex justify-content-center">
                    <button className="btn SQ-btn">Create New Quiz</button>
                </div>
                <div className="row SQ-Questions py-3" style={{background:"#F6F7F9"}}>
                    <Col className="col-md-10" style={{lineHeight:"2"}}>
                        <div className="py-2">
                            <h5>Question 1</h5>
                            <p style={{color:"#302D3A !important",fontFamily:"Poppins",fontSize:"17px !important"}}>What is a difference between Canvas and Artboard in Adobe XD?</p>
                        </div>
                        <div className="py-2">
                            <h5>A</h5>
                            <p>The Canvas can have zero, one or more Artboards placed on it</p>
                        </div>
                        <div className="py-2">
                            <h5>B</h5>
                            <p>Imagine the canvas is a table and each artboard is a sheet of paper. </p>
                        </div>
                        <div className="py-2">
                            <h5>C</h5>
                            <p>You can organize the artboards on the canvas the same way as you can organize .</p>
                        </div>
                        <div className="py-2">
                            <h5>D</h5>
                            <p>All abobe</p>
                        </div>
                    </Col>
                    <Col className="col-md-2 d-flex flex-column justify-content-end">
                        <div className="form-check">
                            <ul>
                                <li><input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" defaultChecked/></li>
                                <li><input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2"/></li>
                                <li><input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault3"/></li>
                                <li><input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault4"/></li>
                            </ul>
                            
                        </div>
                    </Col>
                </div>
                <div className="d-flex justify-content-center py-2">
                    <button className="btn" style={{color:"white",background:"#05CC69",fontSize:"12px",fontFamily:"Poppins"}}>Next Question</button>
                </div>
            </div>
        </>

    )
 
}