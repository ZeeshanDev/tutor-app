import React from 'react'
import { Col, Row } from 'react-bootstrap'
import Quizes from './Components/Quizes/Quizes'
import QuizResult from './Components/QuizResult/QuizResult'
import './MyQuiz.css'

export default function MyQuiz () {


    return (


        <>
            
            <div className="cstm-myQuiz-padding">
                <div className="container MQuiz-container px-0">
                    <div className="cstm-Quiztab bg-white">
                        <ul className="nav nav-tabs" role="tablist">
                            <li className="nav-item col-md-6 px-0">
                                <a className="text-center nav-link active " data-toggle="tab" href="#tabs-10" role="tab">Quizes</a>
                            </li>
                            <li className="nav-item col-md-6 px-0">
                                <a className="text-center nav-link" data-toggle="tab" href="#tabs-11" role="tab">Quiz Result</a>
                            </li>
                        </ul>
                        {/* <!-- Tab panes --> */}
                        <div className="tab-content mt-5">
                            <div className="tab-pane active" id="tabs-10" role="tabpanel">
                                <Quizes />
                                
                                

                            </div>
                            <div className="tab-pane" id="tabs-11" role="tabpanel">
                                <QuizResult />
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </>

    )


}

