import React from 'react'
import { Col, FormLabel, Row } from 'react-bootstrap'
import './NewCourse.css'
import Courseimage from '../../../../assets/images/courseimage.png';
import awesomefilevideo from '../../../../assets/images/awesomefilevideo.png';
import awesomeplus from '../../../../assets/images/awesomeplus.png';

export default function NewCourse () {



    return (
        <div className="container NC-container">
            <div className="NC-R1">
                <Col classname="col-md-5">
                    <Row className=""><img src={Courseimage}></img></Row>
                    <Row>
                        <div><img src={Courseimage}></img></div>
                        
                        <div>
                            <img src={awesomeplus}></img>
                        </div>
                        <div><img src={awesomefilevideo}></img></div>
                        
                    </Row>
                </Col>
                <Col className="form col-md-6">
                    <div className="form-group row">
                        <FormLabel htmlFor="coursename">Course Name</FormLabel>
                        <input type="text" className="form-control" placeholder="" id="coursename" />
                    </div>
                    <div className="form-group row">
                        <Col>
                            <FormLabel htmlFor="coursecategory">Course Category</FormLabel>
                            <select name="year" id="coursecategory" placeholder="1990">
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                            </select>
                        </Col>
                        <Col>
                            <FormLabel htmlFor="language">Language</FormLabel>
                            <select name="year" id="Language" placeholder="1990">
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                            </select>
                        </Col>
                        <Col>
                            <FormLabel htmlFor="courselecture">Course Lecture</FormLabel>
                            <select name="year" id="courselecture" placeholder="1990">
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                            </select>
                        </Col>
                    </div>
                    <div className="form-group row">
                        <FormLabel htmlFor="courseprice">Course Price</FormLabel>
                        <input type="text" className="form-control" placeholder="" id="courseprice" />
                    </div>
                    <div className="form-group row">
                        <FormLabel htmlFor="coursetag">Course Tags</FormLabel>
                        <input type="text" className="form-control" placeholder="" id="coursetag" />
                    </div>
                </Col>
            </div>
            <div className="NC-R2">
                <Col className="col-md-8">
                    <div className="form-group row">
                        <Col>
                            <FormLabel htmlFor="timestart">Time Slot Start</FormLabel>
                            <select name="year" id="timestart" placeholder="12 AM">
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                            </select>
                        </Col>
                        <Col>
                            <FormLabel htmlFor="timeend">Time Slot End</FormLabel>
                            <select name="year" id="timeend" placeholder="12 PM">
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                            </select>
                        </Col>
                        <Col>
                            <FormLabel htmlFor="duration">Duration</FormLabel>
                            <select name="year" id="duration" placeholder="1 month">
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                            </select>
                        </Col>
                    </div>
                </Col>
                <Col className="col-md-4">
                    <div className="row">
                        <h5>Duration</h5>
                    </div>
                    <div className="row">
                        <Col>
                            <input type="radio" name="group1" value="monday"/>Monday <br/>
                            <input type="radio" name="group1" value="wednesday"/>Wednesday <br/>
                            <input type="radio" name="group1" value="friday"/>Friday <br/>
                            <input type="radio" name="group1" value="sunday"/>Sunday <br/>
                        </Col>
                        <Col>
                            <input type="radio" name="group1" value="tuesday"/>Tuesday <br/>
                            <input type="radio" name="group1" value="thursday"/>Thursday <br/>
                            <input type="radio" name="group1" value="saturday"/>Saturday <br/>
                        </Col>
                    </div>
                </Col>
            </div>
            <div className="NC-R3">
                <FormLabel htmlFor="coursedescription">Course Description</FormLabel>
                <textarea id="coursedescription" name="w3review" rows="4" cols="50"></textarea>
            </div>
            <div className="NC-R4">
                <Col className="col-md-8">
                    <div className="form-group row">
                        <FormLabel htmlFor="curriculum">Curriculum</FormLabel>
                        <input type="text" className="form-control" placeholder="" id="curriculum" />
                    </div>
                    <button className="btn">Add More</button>
                </Col>
                <Col className="col-md-3">
                    <FormLabel htmlFor="duration">Duration</FormLabel>
                    <select name="year" id="duration" placeholder="1 month">
                        <option value="1990">1990</option>
                        <option value="1991">1991</option>
                        <option value="1992">1992</option>
                        <option value="1993">1993</option>
                    </select>
                </Col>
            </div>
            <div className="NC-R5">
                <button className="btn">Create Course</button>
            </div>
        </div>
    )

}