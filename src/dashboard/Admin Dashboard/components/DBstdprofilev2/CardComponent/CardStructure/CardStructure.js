import React from 'react'
import './CardStructure.css'
import {Row, Col } from 'react-bootstrap'
import icoption from '../../../../../../images/icoption.png'

const CardStructure = (props) => {
    return (
        <>
            <div className="col-md-2 py-3 px-2">
                <div className="card crd-structure">
                    <div className="card-body d-flex flex-column">
                        <Row>
                            <Col className="d-flex flex-column align-items-center px-1">
                                <div>
                                    <img src={props.image}></img>
                                </div>
                                <div className="py-3">
                                    <h5 className="crdheading mb-0 text-center">{props.name}</h5>
                                    <p className="crdtext">{props.subject}</p>
                                </div>
                                <div className="d-flex flex-column">
                                    <button className="btn button red d-block">Open Profile</button>
                                    <button className="btn button green d-block">Message</button>
                                </div>
                            </Col>
                            <Col className="col-1 px-1">
                                <div>
                                    <img className="dropdown-toggle" aria-haspopup="true" data-toggle="dropdown" src={icoption}></img>
                                    <span className="dropdown-menu cstm-CQuiz-dropdownmenu">
                                        <a className="dropdown-item" href="#">Quiz</a>
                                        <a className="dropdown-item" href="#">Edit</a>
                                        <a className="dropdown-item" href="#">Delete</a>
                                    </span>
                                </div>
                            </Col>
                        </Row>
                        
                    </div>
                </div>
            </div>
            
        </>
    )
}

export default CardStructure
