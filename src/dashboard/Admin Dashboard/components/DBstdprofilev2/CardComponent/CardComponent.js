import React from 'react'
import './CardComponent.css'
import CardStructure from './CardStructure/CardStructure'
import profilePicture111 from '../../../../../images/profilePicture111.png'

const CardComponent = () => {

    const ArrayListCardComponent = [
        {
            id: 1,
            image: profilePicture111,
            name: "Angela Moss",
            subject: "Matemathics",
        },
        {
            id: 2,
            image: profilePicture111,
            name: "Angela Moss",
            subject: "Matemathics",
        },
        {
            id: 3,
            image: profilePicture111,
            name: "Angela Moss",
            subject: "Matemathics",
        },
        {
            id: 4,
            image: profilePicture111,
            name: "Angela Moss",
            subject: "Matemathics",
        },
        {
            id: 5,
            image: profilePicture111,
            name: "Angela Moss",
            subject: "Matemathics",
        },
        {
            id: 6,
            image: profilePicture111,
            name: "Angela Moss",
            subject: "Matemathics",
        },
        {
            id: 7,
            image: profilePicture111,
            name: "Angela Moss",
            subject: "Matemathics",
        },
        {
            id: 8,
            image: profilePicture111,
            name: "Angela Moss",
            subject: "Matemathics",
        },
        {
            id: 9,
            image: profilePicture111,
            name: "Angela Moss",
            subject: "Matemathics",
        },
    ]

     // "arraylistarg" is a variable name which can holds Array current values and "i" is index
     const StdCardList = ArrayListCardComponent.map( (arg, i) => {
        return (
            <CardStructure key={i} 
            id="i"
            image={ArrayListCardComponent[i].image}
            name={ArrayListCardComponent[i].name}
            subject={ArrayListCardComponent[i].subject} 
            
            />
        )
    })

    return (
        <>
            {StdCardList}
        </>
    )
}

export default CardComponent
