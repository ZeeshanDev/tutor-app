import React, {useState} from 'react'
import './Country.css'
import Body from './Component/Body'
import { Col, Row, Form, FormLabel, Modal } from 'react-bootstrap'
import MUIDataTable from "mui-datatables";
import {
  MuiThemeProvider,
  createMuiTheme,
  Tooltip,
  IconButton
} from "@material-ui/core";
import { PersonAdd } from "@material-ui/icons";
import axios from 'axios';


class Country extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        countries: [],
        isOpen: false,
        countryName:"",
        countryCode:""
      };
      
    }

    componentDidMount() {
      axios.get('http://zsktech-001-site1.ctempurl.com/api/country/get')
            .then(response => {
              console.log(response.data.data);
              let arr=[];
              response.data.data.forEach(data => {
                arr.push([data.name, data.code]);
              });
              console.log("thissss",arr);
              this.setState({
                countries: arr
              });
            }
            ).catch(error => 
              console.error(`Error : ${error}`)
            );
    }

    openModal = () => this.setState({ isOpen: true});

    closeModal = () => this.setState({isOpen: false});

    addCountry = () => {
        const country = {
            Name: this.countryName,
            Code: this.countryCode
        };
        axios.post('http://zsktech-001-site1.ctempurl.com/api/country/add', country)
            .then(response => console.log(response.data));
    }

    updateCountryNameValue(countryName) {
      this.setState({
        countryName: countryName.target.value,
      });
      console.log('countryName : '+countryName);
    }
    updateCountryCodeValue(countryCode) {
      this.setState({
        countryCode: countryCode.target.value
      });
      console.log('countcountryCoderyName : '+countryCode);
    }
  
    render() {
      const columns = [
        {
          name: "Country Name",
          options: {
            filter: true,
          }
        },
        {
          label: "Country Code",
          name: "Title",
          options: {
            filter: true,
          }
        },
        {
          // name: "Delete",
          options: {
            filter: true,
            sort: false,
            empty: true,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                // <button onClick={() => {
                //   const { data } = this.state;
                //   data.shift();
                //   this.setState({ data });
                // }}>
                <button className="btn delete-btn btn-danger">
                  Delete
                </button>
              );
            }
          }
        },
        {
          // name: "Edit",
          options: {
            filter: true,
            sort: false,
            empty: true,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <button className="btn edit-btn btn-primary" onClick={this.openModal}>
                  Edit
                </button>
              );
            }
          }
        },
        // {
        //   name: "Add",
        //   options: {
        //     filter: true,
        //     sort: false,
        //     empty: true,
        //     customHeadRender: (columnMeta, handleToggleColumn, sortOrder) => {
        //       return (
        //         // <button onClick={() => {
        //         //   const { data } = this.state;
        //         //   data.unshift(["China", "012"]);
        //         //   this.setState({ data });
        //         // }}>
        //         <button onClick={this.openModal}>
        //           Add
        //         </button>
        //       );
        //     }
        //   }
        // },
      ];

      const AddButton = () => (
        <Tooltip disableFocusListener title="Add User">
          <IconButton onClick={this.openModal}>
            <PersonAdd />
          </IconButton>
        </Tooltip>
      );
  
      // const data1 = [
      //   {CountryName: "Pakistan", CountryCode: "123"},
      //   {CountryName: "UAE", CountryCode: "456"},
      //   {CountryName: "US", CountryCode: "789"},
        
      // ];
  
      const options = {
        filter: true,
        filterType: 'dropdown',
        // responsive: 'stacked',
        responsive: 'scroll',
        customToolbar: AddButton,
        page: 2,
        onColumnSortChange: (changedColumn, direction) => console.log('changedColumn: ', changedColumn, 'direction: ', direction),
        onChangeRowsPerPage: numberOfRows => console.log('numberOfRows: ', numberOfRows),
        onChangePage: currentPage => console.log('currentPage: ', currentPage)
      };

      const getMuiTheme = () =>
      createMuiTheme({
        overrides: {
          MUIDataTableToolbar: {
            actions: {
              display: "flex",
              flex: "initial",
              // move all icons to the right
              "& > span, & > button": {
                order: 99
              },
              // target the custom toolbar icon
              "& > span:last-child, & > button:last-child": {
                order: 1
              },
              // target any icon
              "& > span:nth-child(4), & > button:nth-child(4)": {
                order: 2
              }
            }
          }
        }
      });


        // const [modalState, setModalState] = useState(false);

        // const manageState = () => {
        //     setModalState(!modalState)
        // }

  
      return (
          <>
             <div className="container col-md-12 col-sm-12 col-12 mx-5 py-5 px-4 Country">
                <MuiThemeProvider theme={getMuiTheme()}>
                  <MUIDataTable title={"ACME Employee list"} data={this.state.countries} columns={columns} options={options} />
                </MuiThemeProvider>
              <div>
                        
                        <Modal className="DBStdProfile-Modal" show={this.state.isOpen} onHide={this.closeModal}>
                            <Modal.Header closeButton >
                                <div className="row d-flex px-3">
                                    <div><h5 className="header">Details</h5></div>
                                    
                                </div>
                            </Modal.Header>
                            <Modal.Body>
                                <Form>
                                    
    
                                    <div className="row d-flex justify-content-between">
                                        <div className="d-flex flex-column col">
                                            <FormLabel className="label" htmlFor="countryname">Country Name</FormLabel>
                                            <input type="text" className="form-control border" placeholder="Country Name" id="countryname" />
                                        </div>
                                    </div>
                                    <div className="row d-flex justify-content-between">
                                        <div className="d-flex flex-column col">
                                            <FormLabel className="label" htmlFor="countrycode">Country Code</FormLabel>
                                            <input type="text" className="form-control border" placeholder="Country Code" id="countrycode" />
                                        </div>
                                    </div>
                                </Form>
                            </Modal.Body>
    
                            <Modal.Footer >
                                <div className="row d-flex px-3">
                                    <div><button className="btn btn-success" onClick={this.closeModal}>Save</button></div>
                                    
                                </div>
                            </Modal.Footer>
                            
                            
                        </Modal>
                    </div>
              </div>
          </>
      );
  
    }
  
  }
  
export default Country



// const Country = () => {


//     const columns = ["Country Name", "Country Code"];

//     const data = [
//     ["Pakistan", "123"],
//     ["UAE", "456"],
//     ["USA", "789"],
//     ];

//     const options = {
//     filterType: 'checkbox',
//     };

    // const oneArray = [
    //     {
    //         id: 1,
    //         CountryName: "Pakistan",
    //         CountryCode: "123",
    //     },
    //     {
    //         id: 2,
    //         CountryName: "USA",
    //         CountryCode: "456",
    //     },
    //     {
    //         id: 3,
    //         CountryName: "UAE",
    //         CountryCode: "789",
    //     },
    // ]


    // const oneTableList = oneArray.map( (arg, i) => {
    //     return (
    //         <Body key={i} 
    //         id="i"
    //         ID={oneArray[i].id}
    //         CountryName={oneArray[i].CountryName}
    //         CountryCode={oneArray[i].CountryCode}
    //         />
    //     )
    // })



    // return (
    //     <>
    //         <div className="container p-5 Country">

                
    //                     <MUIDataTable
    //                         title={"Employee List"}
    //                         data={data}
    //                         columns={columns}
    //                         options={options}
    //                     />
                
                
                
                
                
                {/* <table>
                    
                    <thead>
                        <tr>
                            <th scope="col">
                                <div class="form-check">
                                    <label class="form-check-label tableheading" htmlFor="flexCheckDefault">
                                        ID
                                    </label>
                                </div>
                            </th>
                            <th className="tableheading" scope="col">Country Name</th>
                            <th className="tableheading" scope="col">Country Code</th>
                        </tr>
                    </thead>

                    <tbody>
                        {oneTableList}
                    </tbody>

                </table> */}
//             </div>
//         </>
//     )
// }

// export default Country
