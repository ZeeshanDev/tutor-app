import React, {useState} from 'react'
import './Body.css'
import { Col, Row, Button, Form, FormLabel, Modal } from 'react-bootstrap'

const Body = (props) => {

    const [modalState, setModalState] = useState(false);

    const manageState = () => {
        setModalState(!modalState)
    }


    return (
        <>

            <tr className="cstm-border">
                            <td className="py-4 col-2" scope="row">
                                <div class="form-check">
                                    <label class="form-check-label tabletext" htmlFor="flexCheckDefault">
                                        {props.ID}
                                    </label>
                                </div>
                            </td>
                            <td className="tabletext col-4 py-4">{props.CountryName}</td>
                            <td className="pr-5 col-3 tabletext py-4">{props.CountryCode}</td>
                            <td className="py-4 col-3">
                                <span><button className="btn btn-warning py-2 px-4 mr-2" onClick={() => manageState()}>edit</button></span>
                                <span><button className="btn btn-danger py-2 px-3 ml-2">delete</button></span>
                            </td>
            </tr>

            <div>
                        
                        <Modal className="DBStdProfile-Modal" show={modalState} onHide={() => manageState()}>
                            <Modal.Header closeButton >
                                <div className="row d-flex px-3">
                                    <div><h5 className="header">Details</h5></div>
                                    
                                </div>
                            </Modal.Header>
                            <Modal.Body>
                                <Form>
                                    
    
                                    <div className="row d-flex justify-content-between">
                                        <div className="d-flex flex-column col">
                                            <FormLabel className="label" htmlFor="countryname">Country Name</FormLabel>
                                            <input type="text" className="form-control border" placeholder="Country Name" id="countryname" value={this.state.countryName} onChange={this.handleChange}/>
                                        </div>
                                    </div>
                                    <div className="row d-flex justify-content-between">
                                        <div className="d-flex flex-column col">
                                            <FormLabel className="label" htmlFor="countrycode">Country Code</FormLabel>
                                            <input type="text" className="form-control border" placeholder="Country Code" id="countrycode" onChange={countryCode => this.updateCountryCodeValue(countryCode)}/>
                                        </div>
                                    </div>
                                </Form>
                            </Modal.Body>
    
                            <Modal.Footer >
                                <div className="row d-flex px-3">
                                    <div><button className="btn btn-success">Save</button></div>
                                    
                                </div>
                            </Modal.Footer>
                            
                            
                        </Modal>
                    </div>

            
        </>
    )
}

export default Body
