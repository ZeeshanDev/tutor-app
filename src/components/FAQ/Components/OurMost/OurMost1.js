import React, {Component} from 'react';
import './OurMost1.css';
import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import { findDOMNode } from 'react-dom';

class OurMost1 extends Component {

    componentDidMount = () => {


        $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
            $(e.target)
              .prev()
              .find("i:last-child")
              .toggleClass("fa-minus fa-plus");
          });


    }

    render () {
    return (
        <div className="container-fluid OurMost1-containerfluid">
            <div className="container OurMost1-container">
                <div className="col-xl-12 col-lg-12 OurMost1-row1 p-5">
                    <h4>Our most frequently asked questions</h4>
                </div>
            </div>
            <div className="col-xl-12 col-lg-12 OurMost1-row2">
                <div id="accordion" class="myaccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            Which Skillfy for Teams plan is right for me?
                            <span class="fa-stack fa-sm">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                            </span>
                            </button>
                        </h2>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <ul>
                                <li>Computer Science</li>
                                <li>Economics</li>
                                <li>Sociology</li>
                                <li>Nursing</li>
                                <li>English</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )}
}

export default OurMost1;