import React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import Popper from 'popper.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCheckSquare, faCoffee, fashoppingcart} from '@fortawesome/free-solid-svg-icons';
import './NavbarComponent.css';
import searchhere from '../../../images/searchhere.png';
import Cart from '../../../images/Cart.png';
import Logo1 from '../../../images/logo1.png';
import home from '../../Home';
import HomePage from '../../Home';
import history from '../../../History';
import { Link } from 'react-router-dom';
import Home from '../../Home';
import About from '../../AboutUs/AboutUs'
import menu1 from '../../../images/menu1.png'

const Navbar = () => {

    return (
        <nav className="navbar navbar-expand-md text-center container cstm-navbar col-sm-11 col-12">
            <a className="navbar-brand cstm-navbar-brand col-md-2 col-sm-2" href="#"><img src={Logo1}></img></a>
            <div>
                <div className="cstm-nav-item p-2 mobAndtab-hide desktop-hide">
                    <a href="#"><img src={searchhere}></img></a>
                </div>
                <div className="cstm-nav-item p-2 mobAndtab-hide desktop-hide">
                    <Link to='/Cartpage'><img src={Cart}></img></Link>
                </div>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span><img src={menu1}></img></span>
                    {/* <span><i class="fas fa-bars"></i></span> */}
                </button>
            </div>
            <div className="collapse navbar-collapse col-md-7 d-flex justify-content-start" id="collapsibleNavbar">
                <div className="dropdown cstm-navbar-dropdown">

                    {/* --------------mobile and tablet view menu starts----------- */}

                        <div class="panel-group cstm_fAq experimentdiv" id="accordionGroupOpen" role="tablist" aria-multiselectable="true">
                            {/* Home */}

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="home">
                                <h4 class="panel-title px-5">
                                    <span><a href="/" className="cstmHeadingAfterIcon d-inline">Home</a></span>
                                    <a class="collapsed d-inline" role="button" data-toggle="collapse" data-parent="#accordionGroupOpen" href="#collapseOpenHome" aria-expanded="false" aria-controls="collapseHome"/>
                                    
                                </h4>
                                </div>
                                <div id="collapseOpenHome" class="panel-collapse collapse" role="tabpanel" aria-labelledby="home">
                                <div class="panel-body">
                                    <ul className="d-flex flex-column align-items-start">
                                        <Link className="text-black-50" to="/teacherview">Teacher View</Link>
                                        <Link className="text-black-50" to="/studentview">Student View</Link>
                                        <Link className="text-black-50" to="/stdproview">Student Profile View</Link>
                                        <Link className="text-black-50" to="/adboard">Dashboard</Link>
                                        {/* <Link className="text-black-50" to="/country">Country</Link> */}
                                    </ul>
                                </div>
                                </div>
                            </div>
                            
                            {/* About */}
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="about">
                                <h4 class="panel-title px-5">
                                    <span><a href="/AboutUs" className="cstmHeadingAfterIcon d-inline">About</a></span>
                                    <a class="collapsed d-inline" role="button" data-toggle="collapse" data-parent="#accordionGroupOpen" href="#collapseOpenAbout" aria-expanded="false" aria-controls="collapseAbout"/>
                                    
                                </h4>
                                </div>
                                <div id="collapseOpenAbout" class="panel-collapse collapse" role="tabpanel" aria-labelledby="about">
                                <div class="panel-body">
                                    <Link className="text-black-50" to="/termsandconditions">Terms & Conditions</Link>
                                </div>
                                </div>
                            </div>


                            {/* Contact Us */}

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="contact">
                                <h4 class="panel-title px-5">
                                    <span><a href="/ContactUs" className="cstmHeadingAfterIcon d-inline">Contact Us</a></span>
                                    <a class="collapsed d-inline" role="button" data-toggle="collapse" data-parent="#accordionGroupOpen" href="#collapseOpenContact" aria-expanded="false" aria-controls="collapseContact"/>
                                    
                                </h4>
                                </div>
                                <div id="collapseOpenContact" class="panel-collapse collapse" role="tabpanel" aria-labelledby="contact">
                                <div class="panel-body">
                                    <Link className="text-black-50" to="/FAQ">FAQ</Link>
                                </div>
                                </div>
                            </div>

                        </div>

                        

                        {/* -----------------mobile & tablet view menu ends---------- */}

                    <ul className="cstm-ul">
                    
                        <p>
                            
                            <Link to='/'>
                                <button type="button" className="btn dropdown-toggle" aria-haspopup="true" data-toggle="dropdown">
                                Home
                                </button>
                            </Link>
                            <span className="dropdown-menu custom-navbar-dropdownmenu">
                                {/* <a className="dropdown-item" href="/AboutUs">AboutUs</a> */}
                                {/* <a className="dropdown-item" href="/ContactUs">ContactUs</a> */}
                                
                                
                                <a className="dropdown-item" href="/teacherview">TeacherView</a>
                                <a className="dropdown-item" href="/studentview">StudentView</a>
                                <a className="dropdown-item" href="/stdproview">StdProView</a>
                                <a className="dropdown-item" href="/adboard">Dashboard</a>
                                {/* <a className="dropdown-item" href="/country">Country</a> */}

                            </span>
                        </p>
                        <p>
                            <Link to='/AboutUs'>
                                <button type="button" className="btn dropdown-toggle" aria-haspopup="true" data-toggle="dropdown">
                                About
                                </button>
                            </Link>
                            {/* <a href='/AboutUs' className="btn dropdown-toggle" aria-haspopup="true" data-toggle="dropdown">
                                About
                            </a> */}
                            <span className="dropdown-menu custom-navbar-dropdownmenu">
                                <a className="dropdown-item" href="/termsandconditions">Terms & Conditions</a>
                            </span>
                        </p>
                        <p>
                            <Link to='/ContactUs'>
                                <button type="button" className="btn dropdown-toggle" aria-haspopup="true" data-toggle="dropdown">
                                Contact Us
                                </button>
                            </Link>
                            {/* <Link to='/ContactUs' type="button" className="btn dropdown-toggle" data-toggle="dropdown">
                                Contact Us
                            </Link> */}
                            <span className="dropdown-menu custom-navbar-dropdownmenu">
                                <a className="dropdown-item" href="/FAQ">FAQ</a>
                            </span>
                        </p>
                        
                    </ul>
                </div>
            </div>
            
            
            
            
            <div className="col-md-3 collapse navbar-collapse" id="collapsibleNavbar">
                <ul className="navbar-nav align-items-center">
                    <div className="cstm-nav-item p-2 mobAndtab-hide">
                        <a href="#"><img src={searchhere}></img></a>
                    </div>
                    <div className="cstm-nav-item p-2 mobAndtab-hide">
                        <Link to='/Cartpage'><img src={Cart}></img></Link>
                    </div>
                    <div className="cstm-nav-item p-2">
                    <Link to="/Signup" className="btn registerbutton">Register</Link>
                        
                    </div>
                    <div className="cstm-nav-item p-2">
                    <Link to="/Login" className="btn loginbutton">Login</Link>
                        
                    </div>
                </ul>
            </div>  
        </nav>
    )
}

export default Navbar;