import React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import Popper from 'popper.js';
import '../WhatOurStudentsRow2Component/WhatOurStudentsRow2Component.css';

const WhatOurStudentsRow2Component = () => {
    return (
        // <div className="card WhatOurStudents-Row2Component-card bg-success m-4 d-inline-block col-xl-3">
        //     <div className="row WhatOurStudents-Row2Component-card-row p-2">
        //         <img className="rounded-circle" src="https://joeschmoe.io/api/v1/abc" alt="image"></img>
        //         <h4 className="card-card-title flex-grow-1">avatar prac</h4>
        //     </div>
        //     <div className="card-body WhatOurStudents-Row2Component-card-body">
        //         <p className="card-text">Skillfy is a life saver. I don’t have the time <br /> money for a 
        //         college education. My goal <br /> is to become a freelance web designer <br /> and thanks to 
        //         Skillfy.</p>
        //     </div>
        // </div>

        // un-coment it
        <div className="mx-2">
            <div className="cstm-WhatOurStudents-Row2Component p-3">
            <div className="cstm-WhatOurStudents-Row2Component-row-1 p-2">
                <img src="https://joeschmoe.io/api/v1/abc"></img>
                <h5 className="mb-0">Rebecca Moore</h5>
            </div>
            <div className="cstm-WhatOurStudents-Row2Component-row-2 p-2">
                <p className="text-left">Skillfy is a life saver. I don’t have the time money for a 
                   college education. My goal is to become a freelance web designer and thanks to 
                   Skillfy.</p>
            </div>
            </div>
        </div>

        // <div className="card-wrapper">
        //                         <div className="card">
        //                             <div className="card-body">
        //                                 <h5>hello</h5>
        //                             </div>
        //                         </div>
        //                     </div>
    )
}

export default WhatOurStudentsRow2Component;