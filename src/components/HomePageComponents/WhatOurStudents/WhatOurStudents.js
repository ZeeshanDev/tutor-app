import React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import Popper from 'popper.js';
import '../WhatOurStudents/WhatOurStudents.css';
import WhatOurStudentsRow2Component from './WhatOurStudentsRow2Component/WhatOurStudentsRow2Component';
import Layer6 from '../../../images/Layer6.png';
import Layer7 from '../../../images/Layer7.png';

import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import Rotate from 'react-reveal/Rotate';


const WhatOurStudents = () => {

    const settings = {
        dot:true,
        infinite:true,
        speed:500,
        slidesToShow:3,
        slidesToScroll:1,
        cssEase: "linear",
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    speed:500,
                    cssEase: "linear",
                    focusOnSelect: true,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    speed:500,
                    cssEase: "linear",
                    focusOnSelect: true,
                }
            }
        ]
    }

    return (
        <>

            <div className="container-fluid cstm-WhatOurStudents-containerfluid-1 d-flex ">
                <div className="container">
                    <div className="row cstm-WhatOurStudents-row-1  p-4">
                        <h4 className="flex-grow-1 cstm-h4">What our students have to say</h4>
                        {/* <button className="btn cstm-WhatOurStudents-row-1-btn-1 "><img src={Layer6}></img></button>
                        <button className="btn cstm-WhatOurStudents-row-1-btn-2"><img src={Layer7}></img></button> */}
                    </div>
                    <div className="row carousel cstm-carousel">
                        <Slider {...settings}>
                            <Rotate top right>
                                <WhatOurStudentsRow2Component />
                            </Rotate>
                            <Rotate top right>
                                <WhatOurStudentsRow2Component />
                            </Rotate>
                            <Rotate top right>
                                <WhatOurStudentsRow2Component />
                            </Rotate>
                            <Rotate top right>
                                <WhatOurStudentsRow2Component />
                            </Rotate>
                            <Rotate top right>
                                <WhatOurStudentsRow2Component />
                            </Rotate>
                            <Rotate top right>
                                <WhatOurStudentsRow2Component />
                            </Rotate>
                            <Rotate top right>
                                <WhatOurStudentsRow2Component />
                            </Rotate>
                            
                            
                            {/* <div className="col-md-4 col-sm-6">
                            <WhatOurStudentsRow2Component />
                            </div> */}
                        </Slider>
                        {/* <div className="col-md-4 col-sm-6">
                            <WhatOurStudentsRow2Component />
                        </div>
                        <div className="col-md-4 col-sm-6">
                            <WhatOurStudentsRow2Component />
                        </div>
                        <div className="col-md-4 col-sm-6">
                            <WhatOurStudentsRow2Component />
                        </div> */}
                    </div>
                </div>
            </div>

           

        </>

    )
}

export default WhatOurStudents;