import React, {useState} from 'react';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import Popper from 'popper.js';
import '../LearnNewSkill/LearnNewSkill.css';
import bannerpic from '../../../images/bannerpic.png';
import searchbtn from '../../../images/searchbtn.png';
import download1 from '../../../images/download1.jpg'
import download2 from '../../../images/download2.jpg'
import download3 from '../../../images/download3.jpg'
import animatedclipart from '../../../images/animatedclipart.gif'
import animation3 from '../../../images/animation3.gif'

import { useSpring, animated } from 'react-spring'
import BackgroundSlider from 'react-background-slider'



const LearnNewSkill = () => {

    const [searchTerm, setSearchTerm] = useState('');

    const props = useSpring({ from: { opacity: 0 , marginTop: -500 } , to: { opacity: 1 , marginTop:0 } })

    return (
        <>
         {/* <div className="container-fluid header-padding">
             <div className="header-bg">
                  <div className="container cstm-container" >
                     <div className="row align-items-center pt-5">
                         <div className="col-md-6 col-sm-6">
                             <h3 className="banner-heading">Learn new skills online with top educators</h3>
                             <p className="banner-descr">Choose from over 100,000 online video cources with a new additions published every mont.</p>
                            
                             <div className="input-group banner_search mb-0 w-75">
                                 <input type="text" className="form-control" placeholder="Search your favourite courses" />
                                 <div className="input-group-append">
                                     <button className="btn btn-danger btn-sm" type="submit"><img src={searchbtn} className="seacrh_icon_width"></img></button>
                                 </div>
                             </div>

                         </div>
                         <div className="col-md-6 col-sm-6 cstm-banner-image">
                             <img className="img-fluid" src={bannerpic}></img>
                         </div>
                     </div>
                    
                 </div> 
                
             </div>

         </div> */}


        {/*  <animated.div style={props}> */}
        

        <div className="container-fluid learnNewSkill header-padding">
        {/* <BackgroundSlider images={[download1, download2, download3]} duration={10} transition={3} opacity={50}/> */}
            <div className="header-bg">
            {/* <BackgroundSlider images={[download1, download2, download3]} duration={10} transition={3} opacity={50}/> */}
            {/* <div className="header-bg"> */}
                 <div className="container cstm-container" >
                 {/* <animated.div style={props}> */}
                    <div className="row r1 align-items-center pt-5">
                    
                        <div className="col-md-6 col-sm-6">
                            <animated.div style={props}>
                            <h3 className="banner-heading">Learn new skills online with top educators</h3>
                            <p className="banner-descr">Choose from over 100,000 online video cources with a new additions published every mont.</p>
                            
                            <div className="input-group banner_search w-75">
                                <input type="text" className="form-control" onChange={event => {setSearchTerm(event.target.value)}} placeholder="Search your favourite courses" />
                                <div className="input-group-append">
                                    <button className="btn btn-danger btn-sm" type="submit"><img src={searchbtn} className="seacrh_icon_width"></img></button>
                                </div>
                            </div>
                            </animated.div>
                        </div>
                        <div className="col-md-6 col-sm-6 cstm-banner-image">
                            {/* <img className="img-fluid" src={bannerpic}></img> */}
                            {/* <img className="img-fluid" src={animatedclipart}></img> */}
                            <img className="img-fluid" src={animation3}></img>
                            
                        </div>
                    
                    </div>
                 {/* </animated.div> */}
                    
                </div> 
                
            {/* </div> */}
            {/* </BackgroundSlider> */}
            </div>
            {/* {JSONDATA.filter((val) => {
                if(searchTerm == "") {
                    return val
                } else if (val.name.toLocaleLowerCase().includes(searchTerm.toLocaleLowerCase())) {
                    return val
                }
            })} */}

            </div>

                        
         {/* </animated.div> */}

        </>
    )
}

export default LearnNewSkill;