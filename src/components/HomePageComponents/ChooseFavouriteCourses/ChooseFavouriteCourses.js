import React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import Popper from 'popper.js';
import '../ChooseFavouriteCourses/ChooseFavouriteCourses.css';
import ChooseFavouriteRow2Component from './ChooseFavouriteRow2Component/ChooseFavouriteCoursesRow2Component';
import icon8 from '../../../images/icon8.png';
import Layer34 from '../../../images/Layer34.png';
import monitor from '../../../images/monitor.png';
import Layer30 from '../../../images/Layer30.png';
import Layer26 from '../../../images/Layer26.png';
import Layer22 from '../../../images/Layer22.png';
import Layer40 from '../../../images/Layer40.png';
import Layer36 from '../../../images/Layer36.png';
import monitor2 from '../../../images/monitor2.png';
import Layer32 from '../../../images/Layer32.png';
import Layer28 from '../../../images/Layer28.png';
import Layer24 from '../../../images/Layer24.png';

import Jello from 'react-reveal/Jello';

const ChooseFavouriteCourses = () => {
    return (
        <Jello>
        <div className="container ChooseFavouriteCourses mt-5">
            <div className="row align-items-center mb-4">
                <div className="col-md-10">
                    <h4 className="mb-0 heading">Choose favourite courses from top category</h4>
                </div>
                <div className="col-md-2 text-right">
                    <button className="btn button btn-danger btn-md">See all Categories</button>
                </div>
                
                
            </div>
            <div className="row choosefav-cstm-row-2 row-col-sm-2 row-col-md-2 ">
                <div className="col-lg-2 col-md-3 col-sm-6 text-center">
                <ChooseFavouriteRow2Component id="1" name="Design" src={icon8} style={{background:"#EFEFF6"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-6 text-center">
                <ChooseFavouriteRow2Component id="2" name="Development" src={Layer34} style={{background:"#FEF3E8"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-12 text-center">
                <ChooseFavouriteRow2Component id="3" name="IT & Soft" src={monitor} style={{background:"#DDF7FF"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-6 text-center">
                <ChooseFavouriteRow2Component id="4" name="Bussiness" src={Layer30} style={{background:"#E6FAEA"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-6 text-center">
                <ChooseFavouriteRow2Component id="5" name="Marketing" src={Layer26} style={{background:"#EFEFF6"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-8 text-center">
                <ChooseFavouriteRow2Component id="6" name="Photography" src={Layer22} style={{background:"#DDF7FF"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-4 text-center">
                <ChooseFavouriteRow2Component id="7" name="Languages" src={Layer40} style={{background:"#DDF7FF"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-4 text-center">
                <ChooseFavouriteRow2Component id="8" name="Physics" src={Layer36} style={{background:"#EFEFF6"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-8 text-center">
                <ChooseFavouriteRow2Component id="8" name="Chemistry" src={monitor2} style={{background:"#E6FAEA"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-12 text-center">
                <ChooseFavouriteRow2Component id="10" name="Mathematics" src={Layer32} style={{background:"#DDF7FF"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-6 text-center">
                <ChooseFavouriteRow2Component id="11" name="Writing" src={Layer28} style={{background:"#FEF3E8"}}/>
                </div>
                <div className="col-lg-2 col-md-3 col-sm-6 text-center">
                <ChooseFavouriteRow2Component id="12" name="Physocology" src={Layer24} style={{background:"#EFEFF6"}}/>
                </div>
            </div>
        </div>
        </Jello>
    )
}

export default ChooseFavouriteCourses;