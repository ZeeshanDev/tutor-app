import React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import Popper from 'popper.js';
import '../GetYourCourses/GetYourCourses.css';
import ChooseFavouriteRow2Component from '../GetYourCourses/GetYourCoursesRow2component/GetYourCoursesRow2component';

import Zoom from 'react-reveal/Zoom';
import { Link } from 'react-router-dom';

const GetYourCourses = () => {


    

    return (


        // <Zoom left>

            <div className="container-fluid GetYourCourses-containerfluid-1 ">
            <div className="container">
                <div className="row r1 py-4 align-items-center">
                    <div className="col-md-9 c1 my-4">
                        <h4 className="h4-custom mb-0">Get choice of your courses</h4>
                    </div>
                    <div className="col-md-3 c2 d-flex justify-content-between align-items-center">
                        <div className="dropdown GetYourCourses-dropdown">
                            <button type="button" className="btn cstm-button dropdown-toggle" data-toggle="dropdown">
                                Design
                            </button>
                            <div className="dropdown-menu">
                                <a className="dropdown-item" href="#">Link 1</a>
                                <a className="dropdown-item" href="#">Link 2</a>
                                <a className="dropdown-item" href="#">Link 3</a>
                            </div>
                        </div>
                        <Link to='/StdCourse'>
                            <button className="btn btn-danger cstm-button btn-md px-4">View all</button>
                        </Link>
                    </div>
                    
                    
                </div>
                <Zoom left>
                    <div className="row pb-4">
                        <div className="col-md-4 col-sm-6 ">
                            <Link to="/CourseStudent">
                                <ChooseFavouriteRow2Component />
                            </Link>
                        </div>
                        <div className="col-md-4 col-sm-6">
                            <Link to="/CourseStudent">
                                <ChooseFavouriteRow2Component />
                            </Link>
                        </div>
                        <div className="col-md-4 col-sm-6">
                            <Link to="/CourseStudent">
                                <ChooseFavouriteRow2Component />
                            </Link>
                        </div>
                        <div className="col-md-4 col-sm-6">
                            <Link to="/CourseStudent">
                                <ChooseFavouriteRow2Component />
                            </Link>
                        </div>
                        <div className="col-md-4 col-sm-6">
                            <Link to="/CourseStudent">
                                <ChooseFavouriteRow2Component />
                            </Link>
                        </div>
                        <div className="col-md-4 col-sm-6">
                            <Link to="/CourseStudent">
                                <ChooseFavouriteRow2Component />
                            </Link>
                        </div> 
                    </div>
                </Zoom>
                
            </div>
        </div>

        // </Zoom>
            


        
    )
}

export default GetYourCourses;