import React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import Popper from 'popper.js';
import '../GetYourCoursesRow2component/GetYourCoursesRow2component.css';
import Star from '../../../../images/Star.png';
import Layer8 from '../../../../images/Layer8.png';
import Layer9 from '../../../../images/Layer9.png';

const GetYourCoursesRow2Component = () => {
    return (
        <div className="card GetYourCourses-Row2Component-card w-100 mb-4">
                        
                        <div className="card-body  GetYourCourses-Row2Component-card-body">
                            <div className="d-flex justify-content-center"><img className="rounded-circle cstm-img" src="https://joeschmoe.io/api/v1/abc" alt="image"></img></div>
                            <div style={{borderBottom:"#EFEFF6 solid 1px", display:"flex",justifyContent:"space-between", padding:"16px 0px"}}>
                                <span className="d-flex align-items-center" style={{fontSize:"12px",fontFamily:"Poppins"}}>
                                    <img className="mr-2" src={Star} style={{height:"20px", width:"20px"}}></img>4.5(120)
                                </span>
                                <span className="d-flex align-items-center"  style={{fontSize:"12px",fontFamily:"Poppins"}}>
                                    <img className="mr-2" src={Layer8} style={{height:"20px", width:"20px"}}></img>28,500
                                </span>
                                <span className="d-flex align-items-center"  style={{fontSize:"12px",fontFamily:"Poppins"}}>
                                    <img className="mr-2" src={Layer9} style={{height:"20px", width:"20px"}}></img>36 Lesion
                                </span>
                            </div>
                            <h4 className="card-card-title my-3">Statistic Data Science and Bussiness Analysis</h4>
                            <div className="d-flex justify-content-between align-items-center">
                                <div className="d-flex align-items-center">
                                    <span className="mini-img mr-2"><img src="https://joeschmoe.io/api/v1/abc"></img></span>
                                    <span className="userNameTitle">Nicole Brown</span>
                                </div>
                                
                                {/* <span></span>
                                <span></span> */}
                                <div className="d-flex align-items-center">
                                    <span className="prceCut mr-3"><del>$99.99</del></span>
                                    <span className="prceDiscounted" style={{color:"#F68C20"}}>$49.65</span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
    )
}

export default GetYourCoursesRow2Component;