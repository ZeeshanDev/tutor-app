import React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import Popper from 'popper.js';
import '../WeWillHelp/WeWillHelp.css';
import Image from '../../../images/Image.png';

import Jello from 'react-reveal/Jello';

const WeWillHelp = () => {
    return (
        <div className="container-fluid WeWillHelp-container-fluid p-5">
            <div className="container">
                <div className="row WeWillHelp-container-row-1">
                    <div className="col-12">
                    <h4 className="mb-5 helpUsHeading">We will help you learn what you are passionates about</h4>
                    </div>
                    
                    <div className="WeWillHelp-container-row1-column1 col-md-6">
                        <img className="cstm-hide" src={Image}></img>
                        <img className="mobile-responsive col-12 my-2" style={{background: "#cccccc", borderRadius: "15px"}}></img>
                        <img className="mobile-responsive col-12 my-2" style={{background: "#cccccc", borderRadius: "15px"}}></img>
                        <img className="mobile-responsive col-12 my-2" style={{background: "#cccccc", borderRadius: "15px"}}></img>
                    </div>
                    <div className="col-md-6 WeWillHelp-container-row1-column2">
                        <div class="progress-steps">
                            <div class="">
                            <h1 className="h1-cstm">
                                <span class="number">01.</span>
                                Go at your own pace
                            </h1>
                            <p className="steps-description">Find what you are intersted to learn online and choose what exactly best for you that you really passionate to learn and get to study about it</p>
                            </div>
                            <div class="">
                            <h1 className="h1-cstm">
                                <span class="number">02.</span>
                                Learn from industry experts
                            </h1>
                            <p className="steps-description">Find what you are intersted to learn online and choose what exactly best for you that you really passionate to learn and get to study about it</p>
                            </div>
                            <div class="active">
                            <h1 className="h1-cstm">
                                <span class="number">03.</span>
                                Find video courses on almost any topic
                            </h1>
                            <p className="steps-description">Find what you are intersted to learn online and choose what exactly best for you that you really passionate to learn and get to study about it</p>
                            
                            </div>
                        </div>

                    </div>
                </div>
                <div className="row WeWillHelp-container-row-2 ">
                    <div className="WeWillHelp-container-2 p-3">
                        <Jello>
                            <h1 className="h4-custom">It's time to start investing in yourself in learning</h1>
                            <p className="p-custom">Match your goals to our programs, explore your options, and map out your <br />
                            path to success. I plan to use skillfy for a long time!.</p>
                            <button className="btn btn-danger cstm-button px-4">Get Started</button>
                        </Jello>
                        
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WeWillHelp;